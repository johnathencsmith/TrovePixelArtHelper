﻿namespace TrovePixelArtHelper
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.DialogFolderBrowsing = new System.Windows.Forms.FolderBrowserDialog();
            this.TBOutDir = new System.Windows.Forms.TextBox();
            this.BtnOutBrowse = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.TBInImage = new System.Windows.Forms.TextBox();
            this.BtnInBrowse = new System.Windows.Forms.Button();
            this.DialogFileOpen = new System.Windows.Forms.OpenFileDialog();
            this.TBProjectName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.BtnGenerate = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.TBHeight = new System.Windows.Forms.TextBox();
            this.BtnSaveFullIndex = new System.Windows.Forms.Button();
            this.LabelAbout = new System.Windows.Forms.LinkLabel();
            this.GBOptions = new System.Windows.Forms.GroupBox();
            this.CBOpenDirWhenDone = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.CBIndexLocation = new System.Windows.Forms.ComboBox();
            this.CBSaveSeparateIndex = new System.Windows.Forms.CheckBox();
            this.CBSaveConstructionMap = new System.Windows.Forms.CheckBox();
            this.CBSaveTrovizedPixelated = new System.Windows.Forms.CheckBox();
            this.CBSaveTrovized = new System.Windows.Forms.CheckBox();
            this.CBSavePixelated = new System.Windows.Forms.CheckBox();
            this.CBSaveDownscaled = new System.Windows.Forms.CheckBox();
            this.BtnRestoreDefault = new System.Windows.Forms.Button();
            this.DialogFileSave = new System.Windows.Forms.SaveFileDialog();
            this.GBOptions.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "Output Folder: ";
            // 
            // TBOutDir
            // 
            this.TBOutDir.Location = new System.Drawing.Point(113, 6);
            this.TBOutDir.Name = "TBOutDir";
            this.TBOutDir.ReadOnly = true;
            this.TBOutDir.Size = new System.Drawing.Size(552, 21);
            this.TBOutDir.TabIndex = 1;
            // 
            // BtnOutBrowse
            // 
            this.BtnOutBrowse.Location = new System.Drawing.Point(674, 6);
            this.BtnOutBrowse.Name = "BtnOutBrowse";
            this.BtnOutBrowse.Size = new System.Drawing.Size(78, 21);
            this.BtnOutBrowse.TabIndex = 2;
            this.BtnOutBrowse.Text = "Browse...";
            this.BtnOutBrowse.UseVisualStyleBackColor = true;
            this.BtnOutBrowse.Click += new System.EventHandler(this.BtnOutBrowse_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "Input Image: ";
            // 
            // TBInImage
            // 
            this.TBInImage.Location = new System.Drawing.Point(113, 33);
            this.TBInImage.Name = "TBInImage";
            this.TBInImage.ReadOnly = true;
            this.TBInImage.Size = new System.Drawing.Size(552, 21);
            this.TBInImage.TabIndex = 4;
            // 
            // BtnInBrowse
            // 
            this.BtnInBrowse.Location = new System.Drawing.Point(674, 33);
            this.BtnInBrowse.Name = "BtnInBrowse";
            this.BtnInBrowse.Size = new System.Drawing.Size(78, 21);
            this.BtnInBrowse.TabIndex = 5;
            this.BtnInBrowse.Text = "Browse...";
            this.BtnInBrowse.UseVisualStyleBackColor = true;
            this.BtnInBrowse.Click += new System.EventHandler(this.BtnInBrowse_Click);
            // 
            // TBProjectName
            // 
            this.TBProjectName.Location = new System.Drawing.Point(113, 60);
            this.TBProjectName.Name = "TBProjectName";
            this.TBProjectName.Size = new System.Drawing.Size(639, 21);
            this.TBProjectName.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 12);
            this.label3.TabIndex = 7;
            this.label3.Text = "Project name: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(659, 12);
            this.label4.TabIndex = 8;
            this.label4.Text = "Note: Project name cannot contain special characters, a folder for your project w" +
    "ill be automatically created";
            // 
            // BtnGenerate
            // 
            this.BtnGenerate.Font = new System.Drawing.Font("SimSun", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnGenerate.Location = new System.Drawing.Point(305, 126);
            this.BtnGenerate.Name = "BtnGenerate";
            this.BtnGenerate.Size = new System.Drawing.Size(447, 121);
            this.BtnGenerate.TabIndex = 10;
            this.BtnGenerate.Text = "Generate! ";
            this.BtnGenerate.UseVisualStyleBackColor = true;
            this.BtnGenerate.Click += new System.EventHandler(this.BtnGenerate_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 102);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(335, 12);
            this.label5.TabIndex = 12;
            this.label5.Text = "Height in pixel (maximum 255, the sky limit of Trove): ";
            // 
            // TBHeight
            // 
            this.TBHeight.Location = new System.Drawing.Point(353, 99);
            this.TBHeight.Name = "TBHeight";
            this.TBHeight.Size = new System.Drawing.Size(399, 21);
            this.TBHeight.TabIndex = 13;
            this.TBHeight.Text = "150";
            // 
            // BtnSaveFullIndex
            // 
            this.BtnSaveFullIndex.Location = new System.Drawing.Point(305, 253);
            this.BtnSaveFullIndex.Name = "BtnSaveFullIndex";
            this.BtnSaveFullIndex.Size = new System.Drawing.Size(447, 41);
            this.BtnSaveFullIndex.TabIndex = 17;
            this.BtnSaveFullIndex.Text = "Save Full Blocks Index As .txt File";
            this.BtnSaveFullIndex.UseVisualStyleBackColor = true;
            this.BtnSaveFullIndex.Click += new System.EventHandler(this.BtnSaveFullIndex_Click);
            // 
            // LabelAbout
            // 
            this.LabelAbout.AutoSize = true;
            this.LabelAbout.Location = new System.Drawing.Point(483, 310);
            this.LabelAbout.Name = "LabelAbout";
            this.LabelAbout.Size = new System.Drawing.Size(269, 12);
            this.LabelAbout.TabIndex = 18;
            this.LabelAbout.TabStop = true;
            this.LabelAbout.Text = "By VanillaMeow - Click to enter project page";
            this.LabelAbout.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LabelAbout_LinkClicked);
            // 
            // GBOptions
            // 
            this.GBOptions.Controls.Add(this.CBOpenDirWhenDone);
            this.GBOptions.Controls.Add(this.label6);
            this.GBOptions.Controls.Add(this.CBIndexLocation);
            this.GBOptions.Controls.Add(this.CBSaveSeparateIndex);
            this.GBOptions.Controls.Add(this.CBSaveConstructionMap);
            this.GBOptions.Controls.Add(this.CBSaveTrovizedPixelated);
            this.GBOptions.Controls.Add(this.CBSaveTrovized);
            this.GBOptions.Controls.Add(this.CBSavePixelated);
            this.GBOptions.Controls.Add(this.CBSaveDownscaled);
            this.GBOptions.Location = new System.Drawing.Point(14, 117);
            this.GBOptions.Name = "GBOptions";
            this.GBOptions.Size = new System.Drawing.Size(285, 215);
            this.GBOptions.TabIndex = 19;
            this.GBOptions.TabStop = false;
            this.GBOptions.Text = "Options";
            // 
            // CBOpenDirWhenDone
            // 
            this.CBOpenDirWhenDone.AutoSize = true;
            this.CBOpenDirWhenDone.Checked = true;
            this.CBOpenDirWhenDone.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CBOpenDirWhenDone.Location = new System.Drawing.Point(7, 185);
            this.CBOpenDirWhenDone.Name = "CBOpenDirWhenDone";
            this.CBOpenDirWhenDone.Size = new System.Drawing.Size(150, 16);
            this.CBOpenDirWhenDone.TabIndex = 8;
            this.CBOpenDirWhenDone.Text = "Open folder when done";
            this.CBOpenDirWhenDone.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 162);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 12);
            this.label6.TabIndex = 7;
            this.label6.Text = "Index location: ";
            // 
            // CBIndexLocation
            // 
            this.CBIndexLocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CBIndexLocation.Enabled = false;
            this.CBIndexLocation.FormattingEnabled = true;
            this.CBIndexLocation.Items.AddRange(new object[] {
            "Top",
            "Bottom",
            "Left",
            "Right"});
            this.CBIndexLocation.Location = new System.Drawing.Point(113, 159);
            this.CBIndexLocation.Name = "CBIndexLocation";
            this.CBIndexLocation.Size = new System.Drawing.Size(158, 20);
            this.CBIndexLocation.TabIndex = 6;
            // 
            // CBSaveSeparateIndex
            // 
            this.CBSaveSeparateIndex.AutoSize = true;
            this.CBSaveSeparateIndex.Checked = true;
            this.CBSaveSeparateIndex.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CBSaveSeparateIndex.Enabled = false;
            this.CBSaveSeparateIndex.Location = new System.Drawing.Point(7, 136);
            this.CBSaveSeparateIndex.Name = "CBSaveSeparateIndex";
            this.CBSaveSeparateIndex.Size = new System.Drawing.Size(264, 16);
            this.CBSaveSeparateIndex.TabIndex = 5;
            this.CBSaveSeparateIndex.Text = "Save block index in a separate .txt file";
            this.CBSaveSeparateIndex.UseVisualStyleBackColor = true;
            this.CBSaveSeparateIndex.CheckedChanged += new System.EventHandler(this.CBSaveSeparateIndex_CheckedChanged);
            // 
            // CBSaveConstructionMap
            // 
            this.CBSaveConstructionMap.AutoSize = true;
            this.CBSaveConstructionMap.Checked = true;
            this.CBSaveConstructionMap.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CBSaveConstructionMap.Enabled = false;
            this.CBSaveConstructionMap.Location = new System.Drawing.Point(7, 113);
            this.CBSaveConstructionMap.Name = "CBSaveConstructionMap";
            this.CBSaveConstructionMap.Size = new System.Drawing.Size(150, 16);
            this.CBSaveConstructionMap.TabIndex = 4;
            this.CBSaveConstructionMap.Text = "Save construction map";
            this.CBSaveConstructionMap.UseVisualStyleBackColor = true;
            // 
            // CBSaveTrovizedPixelated
            // 
            this.CBSaveTrovizedPixelated.AutoSize = true;
            this.CBSaveTrovizedPixelated.Checked = true;
            this.CBSaveTrovizedPixelated.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CBSaveTrovizedPixelated.Location = new System.Drawing.Point(7, 90);
            this.CBSaveTrovizedPixelated.Name = "CBSaveTrovizedPixelated";
            this.CBSaveTrovizedPixelated.Size = new System.Drawing.Size(198, 16);
            this.CBSaveTrovizedPixelated.TabIndex = 3;
            this.CBSaveTrovizedPixelated.Text = "Save Trovized pixelated image";
            this.CBSaveTrovizedPixelated.UseVisualStyleBackColor = true;
            // 
            // CBSaveTrovized
            // 
            this.CBSaveTrovized.AutoSize = true;
            this.CBSaveTrovized.Location = new System.Drawing.Point(7, 67);
            this.CBSaveTrovized.Name = "CBSaveTrovized";
            this.CBSaveTrovized.Size = new System.Drawing.Size(138, 16);
            this.CBSaveTrovized.TabIndex = 2;
            this.CBSaveTrovized.Text = "Save Trovized image";
            this.CBSaveTrovized.UseVisualStyleBackColor = true;
            // 
            // CBSavePixelated
            // 
            this.CBSavePixelated.AutoSize = true;
            this.CBSavePixelated.Location = new System.Drawing.Point(7, 44);
            this.CBSavePixelated.Name = "CBSavePixelated";
            this.CBSavePixelated.Size = new System.Drawing.Size(144, 16);
            this.CBSavePixelated.TabIndex = 1;
            this.CBSavePixelated.Text = "Save pixelated image";
            this.CBSavePixelated.UseVisualStyleBackColor = true;
            // 
            // CBSaveDownscaled
            // 
            this.CBSaveDownscaled.AutoSize = true;
            this.CBSaveDownscaled.Location = new System.Drawing.Point(7, 21);
            this.CBSaveDownscaled.Name = "CBSaveDownscaled";
            this.CBSaveDownscaled.Size = new System.Drawing.Size(150, 16);
            this.CBSaveDownscaled.TabIndex = 0;
            this.CBSaveDownscaled.Text = "Save downscaled image";
            this.CBSaveDownscaled.UseVisualStyleBackColor = true;
            // 
            // BtnRestoreDefault
            // 
            this.BtnRestoreDefault.Location = new System.Drawing.Point(306, 301);
            this.BtnRestoreDefault.Name = "BtnRestoreDefault";
            this.BtnRestoreDefault.Size = new System.Drawing.Size(171, 31);
            this.BtnRestoreDefault.TabIndex = 20;
            this.BtnRestoreDefault.Text = "Restore default options";
            this.BtnRestoreDefault.UseVisualStyleBackColor = true;
            this.BtnRestoreDefault.Click += new System.EventHandler(this.BtnRestoreDefault_Click);
            // 
            // FormMain
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(764, 340);
            this.Controls.Add(this.BtnRestoreDefault);
            this.Controls.Add(this.GBOptions);
            this.Controls.Add(this.LabelAbout);
            this.Controls.Add(this.BtnSaveFullIndex);
            this.Controls.Add(this.TBHeight);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.BtnGenerate);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TBProjectName);
            this.Controls.Add(this.BtnInBrowse);
            this.Controls.Add(this.TBInImage);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.BtnOutBrowse);
            this.Controls.Add(this.TBOutDir);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Trove Pixel Art Helper";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.FormMain_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.FormMain_DragEnter);
            this.GBOptions.ResumeLayout(false);
            this.GBOptions.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FolderBrowserDialog DialogFolderBrowsing;
        private System.Windows.Forms.TextBox TBOutDir;
        private System.Windows.Forms.Button BtnOutBrowse;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TBInImage;
        private System.Windows.Forms.Button BtnInBrowse;
        private System.Windows.Forms.OpenFileDialog DialogFileOpen;
        private System.Windows.Forms.TextBox TBProjectName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button BtnGenerate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TBHeight;
        private System.Windows.Forms.Button BtnSaveFullIndex;
        private System.Windows.Forms.LinkLabel LabelAbout;
        private System.Windows.Forms.GroupBox GBOptions;
        private System.Windows.Forms.CheckBox CBSaveConstructionMap;
        private System.Windows.Forms.CheckBox CBSaveTrovizedPixelated;
        private System.Windows.Forms.CheckBox CBSavePixelated;
        private System.Windows.Forms.CheckBox CBSaveDownscaled;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox CBIndexLocation;
        private System.Windows.Forms.CheckBox CBSaveSeparateIndex;
        private System.Windows.Forms.CheckBox CBOpenDirWhenDone;
        private System.Windows.Forms.Button BtnRestoreDefault;
        private System.Windows.Forms.SaveFileDialog DialogFileSave;
        private System.Windows.Forms.CheckBox CBSaveTrovized;
    }
}

