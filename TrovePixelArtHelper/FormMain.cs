using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace TrovePixelArtHelper
{
    public partial class FormMain : Form
    {
        /// <summary>
        /// Form constructor
        /// </summary>
        public FormMain()
        {
            InitializeComponent();
        }

        #region Constants and Global Variables

        /// <summary>
        /// The title of this software
        /// </summary>
        public const string TITLE = "Trove Pixel Art Helper v1.4";

        /// <summary>
        /// The GitLab project page
        /// </summary>
        public const string PROJECT_PAGE = "https://gitlab.com/x01x012015/TrovePixelArtHelper";

        /// <summary>
        /// Block struct
        /// This is a block that exist in-game
        /// </summary>
        private struct Block
        {
            public string name;
            public Color color;
        }

        /// <summary>
        /// This will hold every Block that exist in-game
        /// </summary>
        private Block[] blocks;

        #endregion

        #region Event Handlers

        /// <summary>
        /// Form load, initialization
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormMain_Load(object sender, EventArgs e)
        {
            //Show version on title
            this.Text = TITLE;
            //Set default values to some textboxes
            TBOutDir.Text = Application.StartupPath;
            //Load in-game block colors
            //The new line character of resource file should not change when changing platform, however, it is not tested. The following line works on Windows
            string[] InGameColors = Properties.Resources.InGameBlockColors.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            //Initialize Block array
            blocks = new Block[InGameColors.Count()];
            for (int i = 0; i < InGameColors.Count(); i++)
            {
                //Temporary variable temp will have structure like this: [name, r, g, b]
                string[] temp = InGameColors[i].Split('\t');
                //Initialize this Block
                blocks[i] = new Block
                {
                    name = temp[0],
                    color = Color.FromArgb(int.Parse(temp[1]), int.Parse(temp[2]), int.Parse(temp[3]))
                };
            }
            //Set default options
            BtnRestoreDefault.PerformClick();
        }

        /// <summary>
        /// Change mouse pointer when a file is dragged onto the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">Event argument, used to check if item dragged onto the form is a file</param>
        private void FormMain_DragEnter(object sender, DragEventArgs e)
        {
            try
            {
                //Get dragged file path
                string droppedFilePath = ((string[])e.Data.GetData(DataFormats.FileDrop))[0];
                //Check if it is a directory
                if (!File.GetAttributes(droppedFilePath).HasFlag(FileAttributes.Directory))
                {
                    //It is a file, change mouse pointer
                    e.Effect = DragDropEffects.Copy;
                }
            }
            catch (Exception err) when (err is NullReferenceException || err is ArgumentException || err is PathTooLongException || err is FileNotFoundException || err is DirectoryNotFoundException || err is IOException || err is UnauthorizedAccessException)
            {
                //Fail silently
            }
        }

        /// <summary>
        /// Load path to the file that is dropped
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">Event argument, used to get file path</param>
        private void FormMain_DragDrop(object sender, DragEventArgs e)
        {
            try
            {
                //Get dropped file path
                string droppedFilePath = ((string[])e.Data.GetData(DataFormats.FileDrop))[0];
                //Check if it is a directory
                if (!File.GetAttributes(droppedFilePath).HasFlag(FileAttributes.Directory))
                {
                    //It is a file, put data into textboxes
                    TBInImage.Text = droppedFilePath;
                    TBProjectName.Text = Path.GetFileNameWithoutExtension(droppedFilePath);
                }
            }
            catch (Exception err) when (err is NullReferenceException || err is ArgumentException || err is PathTooLongException || err is FileNotFoundException || err is DirectoryNotFoundException || err is IOException || err is UnauthorizedAccessException)
            {
                //Fail silently
            }
        }

        /// <summary>
        /// Output folder browsing button
        /// Let user pick an output folder
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnOutBrowse_Click(object sender, EventArgs e)
        {
            if (DialogFolderBrowsing.ShowDialog() == DialogResult.OK)
            {
                TBOutDir.Text = DialogFolderBrowsing.SelectedPath;
            }
        }

        /// <summary>
        /// Input file browsing button
        /// Let user pick an input file
        /// This method will also put default prefix into the textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnInBrowse_Click(object sender, EventArgs e)
        {
            DialogFileOpen.Filter = "Supported Formats|*.bmp;*.gif;*.jpg;*.png;*.tiff|All Files|*.*";
            if (DialogFileOpen.ShowDialog() == DialogResult.OK)
            {
                TBInImage.Text = DialogFileOpen.FileName;
                TBProjectName.Text = Path.GetFileNameWithoutExtension(DialogFileOpen.FileName);
            }
        }

        /// <summary>
        /// Main button
        /// The implementation will be in Core Code section
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnGenerate_Click(object sender, EventArgs e)
        {
            //Update title
            this.Text = TITLE + " - Please wait... ";
            Application.DoEvents();
            //Call core functions
            string projectFolder;
            if (ValidateInputs(out projectFolder))
            {
                Generate(projectFolder);
            }
            //Update title
            this.Text = TITLE;
        }

        /// <summary>
        /// Save full block index
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSaveFullIndex_Click(object sender, EventArgs e)
        {
            //Prepare file save dialog
            DialogFileSave.Filter = "Text Files|*.txt|All Files|*.*";
            DialogFileSave.FileName = "Full Blocks Index.txt";
            //Show dialog and see if "Save" is clicked
            if (DialogFileSave.ShowDialog() == DialogResult.OK)
            {
                //Prepare data to write
                string[] lines = new string[blocks.Count()];
                //Loop thorugh blocks and prepare lines to write out
                for (int i = 0; i < blocks.Count(); i++)
                {
                    lines[i] = ToTwoDigitHex(i) + " - " + blocks[i].name;
                }
                //Write lines out
                try
                {
                    //Write lines
                    File.WriteAllLines(DialogFileSave.FileName, lines);
                    //Launch folder when finished if needed
                    if (CBOpenDirWhenDone.Checked)
                    {
                        Process.Start(TBOutDir.Text);
                    }
                }
                catch (Exception err) when (err is IOException || err is UnauthorizedAccessException)
                {
                    //Couldn't save file, notify the user
                    MessageBox.Show("Failed to save full index, error message: " + err.Message);
                }
            }
        }

        /// <summary>
        /// Restore options to their default state
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnRestoreDefault_Click(object sender, EventArgs e)
        {
            TBHeight.Text = "150";
            CBSaveDownscaled.Checked = false;
            CBSavePixelated.Checked = false;
            CBSaveTrovized.Checked = false;
            CBSaveTrovizedPixelated.Checked = true;
            CBSaveConstructionMap.Checked = true;
            CBSaveSeparateIndex.Checked = true;
            CBIndexLocation.SelectedItem = "Bottom";
            CBOpenDirWhenDone.Checked = true;
        }

        /// <summary>
        /// Take user to project page when about label is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LabelAbout_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(PROJECT_PAGE);
        }

        /// <summary>
        /// Save index to separate file checkbox state change hander
        /// Enable or disable index location dropdown box accordingly
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CBSaveSeparateIndex_CheckedChanged(object sender, EventArgs e)
        {
            CBIndexLocation.Enabled = !CBSaveSeparateIndex.Checked;
        }

        #endregion

        #region Core Code

        /// <summary>
        /// Validate input fields
        /// This method will also try to create project folder then validate other inputs
        /// Error messages will be shown to the user if needed
        /// </summary>
        /// <param name="projectFolder">The path of the project folder will be returned, if there is an error, an empty string will be returned</param>
        /// <returns>true if tests passed, false otherwise</returns>
        private bool ValidateInputs(out string projectFolder)
        {
            //Combine project folder path
            try
            {
                projectFolder = Path.Combine(TBOutDir.Text, TBProjectName.Text);
            }
            catch (ArgumentException)
            {
                //We must assign out parameter
                projectFolder = "";
                //Show error message to the user
                MessageBox.Show("Error: Your project name contains special characters. ");
                //Focus textbox and return
                TBProjectName.Focus();
                return false;
            }
            //Check if project folder is already there, and ask user for confirmation
            if (Directory.Exists(projectFolder) && MessageBox.Show("Warning: Project folder already exists, do you wish to continue anyway and overwrite existing files? ", "", MessageBoxButtons.YesNo) == DialogResult.No)
            {
                return false;
            }
            //Create project folder
            try
            {
                Directory.CreateDirectory(projectFolder);
            }
            catch (Exception err) when (err is IOException || err is UnauthorizedAccessException || err is DirectoryNotFoundException)
            {
                //Could not write
                MessageBox.Show("Error: Could not create project folder, please check if you have write permission. ");
                //Focus textbox and return
                TBProjectName.Focus();
                return false;
            }
            catch (Exception err) when (err is ArgumentException)
            {
                //Path is not valid
                MessageBox.Show("Error: Your project name contains special characters. ");
                //Focus textbox and return
                TBProjectName.Focus();
                return false;
            }
            catch (Exception err) when (err is PathTooLongException)
            {
                //Path is too long
                MessageBox.Show("Error: Either output folder path or project name is too long. ");
                //Focus textbox and return
                TBProjectName.Focus();
                return false;
            }
            //Check if entered height is valid, we will check if entered height is lower than original image height later
            int height;
            try
            {
                height = int.Parse(TBHeight.Text);
            }
            catch (Exception err) when (err is FormatException || err is OverflowException)
            {
                //Height is not valid
                MessageBox.Show("Error: Height is not a valid integer. ");
                //Focus textbox and return
                TBHeight.Focus();
                return false;
            }
            //Check if height is too large or too small
            if (height < 1)
            {
                height = 1;
            }
            else if (height > 255)
            {
                height = 255;
            }
            //Write back to the textbox for future use
            TBHeight.Text = height.ToString();
            //Update UI
            Application.DoEvents();
            //Everything is right
            return true;
        }

        /// <summary>
        /// The core of this software, everything about generating images is handled here
        /// </summary>
        /// <param name="projectFolder">The path to project folder, this should have been created before</param>
        private void Generate(string projectFolder)
        {
            //Load image and downscale it, the downscaled image will be needed later on
            Bitmap downscaledImage;
            try
            {
                using (Image imgFromFile = Image.FromFile(TBInImage.Text))
                {
                    //This is validated before and won't fail
                    int height = int.Parse(TBHeight.Text);
                    //Check if origonal image is too small
                    if (imgFromFile.Height < height && MessageBox.Show("Warning: Image that you selected is smaller than the height you specified, do you wish to upscale it? ", "", MessageBoxButtons.YesNo) == DialogResult.No)
                    {
                        height = imgFromFile.Height;
                        //Write back to textbox for clarity
                        TBHeight.Text = height.ToString();
                        //Update UI
                        Application.DoEvents();
                    }
                    //Downscale image
                    downscaledImage = new Bitmap(ScaleImageWithInterpolation(imgFromFile, int.MaxValue, height));
                }
            }
            catch (OutOfMemoryException)
            {
                //Unlike what the exception says, this happens when the file is not supported
                MessageBox.Show("Error: Input file is not a valid image. ");
                //We don't need to dispose downscaledImage since it is not yet created
                return;
            }
            catch (FileNotFoundException)
            {
                //This will happen when the user removed selected file
                MessageBox.Show("Error: Cannot open input file. ");
                //We don't need to dispose downscaledImage since it is not yet created
                return;
            }
            //Save the downscaled image if needed
            if (CBSaveDownscaled.Checked)
            {
                try
                {
                    downscaledImage.Save(Path.Combine(projectFolder, "Original Downscaled.png"), ImageFormat.Png);
                }
                catch (System.Runtime.InteropServices.ExternalException)
                {
                    //Couldn't save the file
                    MessageBox.Show("Error: Cannot save downscaled image. ");
                }
            }
            //Save pixelated image if needed
            if (CBSavePixelated.Checked)
            {
                using (Bitmap pixelatedImage = ScaleImageWithoutInterpolation(downscaledImage, 10))
                {
                    try
                    {
                        pixelatedImage.Save(Path.Combine(projectFolder, "Original Pixelated.png"), ImageFormat.Png);
                    }
                    catch (System.Runtime.InteropServices.ExternalException)
                    {
                        //Couldn't save the file
                        MessageBox.Show("Error: Cannot save pixelated image. ");
                    }
                }
            }
            //Trovize image, we will overwrite downscaledImage since it is no longer needed
            //This matrix will store the index of the block that is needed for each pixel
            int[,] constructionPlanMatrix = new int[downscaledImage.Width, downscaledImage.Height];
            //Loop though the whole image and convert it into colors that exists in-game
            for (int x = 0; x < downscaledImage.Width; x++)
            {
                for (int y = 0; y < downscaledImage.Height; y++)
                {
                    //For each pixel, we need to find the closest color and save it
                    int bestColorIndex = 0;
                    double currentDiff = double.MaxValue;
                    //Color of current pixel
                    Color currentPixelColor = downscaledImage.GetPixel(x, y);
                    for (int i = 0; i < blocks.Count(); i++)
                    {
                        //See if we have a closer color
                        double cmpResult = ColorCmp(currentPixelColor, blocks[i].color);
                        if (cmpResult < currentDiff)
                        {
                            bestColorIndex = i;
                            currentDiff = cmpResult;
                        }
                    }
                    //Found best color, save it
                    downscaledImage.SetPixel(x, y, blocks[bestColorIndex].color);
                    constructionPlanMatrix[x, y] = bestColorIndex;
                }
            }
            //Save Trovized image if needed
            if (CBSaveTrovized.Checked)
            {
                try
                {
                    downscaledImage.Save(Path.Combine(projectFolder, "Trovized Downscaled.png"), ImageFormat.Png);
                }
                catch (System.Runtime.InteropServices.ExternalException)
                {
                    //Couldn't save the file
                    MessageBox.Show("Error: Cannot save Trovized image. ");
                }
            }
            //Save Trovized pixelated image if needed
            if (CBSaveTrovizedPixelated.Checked)
            {
                using (Bitmap pixelatedImage = ScaleImageWithoutInterpolation(downscaledImage, 10))
                {
                    try
                    {
                        pixelatedImage.Save(Path.Combine(projectFolder, "Trovized Pixelated.png"), ImageFormat.Png);
                    }
                    catch (System.Runtime.InteropServices.ExternalException)
                    {
                        //Couldn't save the file
                        MessageBox.Show("Error: Cannot save Trovized pixelated image. ");
                    }
                }
            }
            //Save construction plan, by design, this must be saved, so we will not check if the checkbox is checked; it should not be enabled anyway
            //Generate construction plan hexadecimal matrix
            string[,] constructionPlanHex = new string[downscaledImage.Width, downscaledImage.Height];
            //Loop through the whole image and convert indexes to hexadecimal
            for (int x = 0; x < downscaledImage.Width; x++)
            {
                for (int y = 0; y < downscaledImage.Height; y++)
                {
                    constructionPlanHex[x, y] = ToTwoDigitHex(constructionPlanMatrix[x, y]);
                }
            }
            //Scale up image by 50 times and put numbers on each square
            using (Bitmap constructionPlanImage = ScaleImageWithoutInterpolation(downscaledImage, 50))
            using (Graphics constructionPlanGraphics = Graphics.FromImage(constructionPlanImage))
            {
                //Draw grid
                using (Pen blackPen = new Pen(Color.Black, 2))
                {
                    //Vertical lines
                    for (int x = 49; x < constructionPlanImage.Width - 5; x += 50)
                    {
                        constructionPlanGraphics.DrawLine(blackPen, new Point(x, 0), new Point(x, constructionPlanImage.Height));
                    }
                    //Horizontal lines
                    for (int y = 49; y < constructionPlanImage.Height - 5; y += 50)
                    {
                        constructionPlanGraphics.DrawLine(blackPen, new Point(0, y), new Point(constructionPlanImage.Width, y));
                    }
                }
                //Draw index numbers
                using (Font consolasFont = new Font("Consolas", 24))
                {
                    //The original y location to draw
                    const int yDrawOriginal = 5;
                    //Keep track of location to draw
                    int xDraw = 1;
                    int yDraw;
                    //Loop through each pixel
                    for (int x = 0; x < downscaledImage.Width; x++)
                    {
                        //Reset y location
                        yDraw = yDrawOriginal;
                        for (int y = 0; y < downscaledImage.Height; y++)
                        {
                            //Draw index
                            constructionPlanGraphics.DrawString(constructionPlanHex[x, y], consolasFont, new SolidBrush(ContrastColor(downscaledImage.GetPixel(x, y))), xDraw, yDraw);
                            //Update location to draw
                            yDraw += 50;
                        }
                        //Update x location
                        xDraw += 50;
                    }

                }
                //Save image
                try
                {
                    constructionPlanImage.Save(Path.Combine(projectFolder, "Construction Plan.png"), ImageFormat.Png);
                }
                catch (System.Runtime.InteropServices.ExternalException)
                {
                    //Couldn't save the file
                    MessageBox.Show("Error: Cannot save construction plan. ");
                }
            }
            //Save used indexes with used times count
            //Initialize counter
            int[] blocksCounter = Enumerable.Repeat(0, blocks.Count()).ToArray();
            //Loop through the construction plan matrix and count each index
            for (int x = 0; x < downscaledImage.Width; x++)
            {
                for (int y = 0; y < downscaledImage.Height; y++)
                {
                    blocksCounter[constructionPlanMatrix[x, y]]++;
                }
            }
            //Prepare lines to write
            string[] lines = new string[blocksCounter.Count(i => i != 0)];
            //Since we are not outputting all the lines, we need a separate counter
            int linesIndex = 0;
            //Loop though each counter and prepare lines to write
            for (int i = 0; i < blocksCounter.Count(); i++)
            {
                //Check if the block is used
                if (blocksCounter[i] > 0)
                {
                    //Write line
                    lines[linesIndex++] = ToTwoDigitHex(i) + " - " + blocks[i].name + "  (" + blocksCounter[i] + ")";
                }
            }
            //Write to file
            try
            {
                File.WriteAllLines(Path.Combine(projectFolder, "Construction Block Index And Usage.txt"), lines);
            }
            catch (Exception err) when (err is PathTooLongException)
            {
                //Couldn't save the file
                MessageBox.Show("Error: Cannot save construction plan index and usage because either output folder path or project name is too long. ");
            }
            catch (Exception err) when (err is IOException || err is UnauthorizedAccessException)
            {
                //Couldn't save the file
                MessageBox.Show("Error: Cannot save construction plan index and usage because project folder is not writtable. ");
            }
            //Dispose downscaledImage
            downscaledImage.Dispose();
            //Launch folder when finished if needed
            if (CBOpenDirWhenDone.Checked)
            {
                Process.Start(TBOutDir.Text);
            }
        }

        #endregion

        #region Static Library Functions

        /// <summary>
        /// Compare two color with slightly modified CIE76 algorithm
        /// Documentation: https://en.wikipedia.org/wiki/Color_difference
        /// We did not square root the result, since it will not change the result and will speed up our code
        /// </summary>
        /// <param name="col1">First color</param>
        /// <param name="col2">Second color</param>
        /// <returns>Relative color difference</returns>
        private static double ColorCmp(Color col1, Color col2)
        {
            return Math.Pow((col1.R - col2.R), 2) + Math.Pow((col1.G - col2.G), 2) + Math.Pow((col1.B - col2.B), 2);
        }

        /// <summary>
        /// Convert a number into two digit hexadecimal string
        /// This is used to convert decimal index into a two character string when doing output to improve consistency
        /// </summary>
        /// <param name="i">The number to convert</param>
        /// <returns>Two deigit hexadecimal string</returns>
        private static string ToTwoDigitHex(int i)
        {
            //Convert into hexadecimal
            string temp = i.ToString("X");
            return (temp.Length == 1) ? "0" + temp : temp;
        }

        /// <summary>
        /// Find what should be the font color
        /// This is written by Gacek of Stackoverflow and is unchanged
        /// http://stackoverflow.com/questions/1855884/determine-font-color-based-on-background-color
        /// </summary>
        /// <param name="color">Background color</param>
        /// <returns>A good font color for input background color</returns>
        private static Color ContrastColor(Color color)
        {
            int d = 0;

            // Counting the perceptive luminance - human eye favors green color... 
            double a = 1 - (0.299 * color.R + 0.587 * color.G + 0.114 * color.B) / 255;

            if (a < 0.5)
                d = 0; // bright colors - black font
            else
                d = 255; // dark colors - white font

            return Color.FromArgb(d, d, d);
        }

        /// <summary>
        /// Scalling an image without interpolation but keeping ratio
        /// The built-in scalling functionality does not bahave the way we want, the border of the image will copy "imaginary" pixels outside the image
        /// </summary>
        /// <param name="originalImage">Original bitmap</param>
        /// <param name="ratio">Scalling ratio</param>
        /// <returns>A scaled bitmap</returns>
        private static Bitmap ScaleImageWithoutInterpolation(Bitmap originalImage, int ratio)
        {
            Bitmap newImage;
            //Temporary workaround: exit if cannot create bitmap
            try
            {
                //Initialize output bitmap
                newImage = new Bitmap(originalImage.Width * ratio, originalImage.Height * ratio);
            }
            catch (ArgumentException)
            {
                //Cannot create bitmap in memory
                MessageBox.Show("Output image is too large for the current implementation to handle, generation failed and this software will exit now. ", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(0);
                throw new NotImplementedException();
            }
            //Draw each pixel
            using (Graphics newImageGraphics = Graphics.FromImage(newImage))
            {
                for (int x = 0; x < originalImage.Width; x++)
                {
                    for (int y = 0; y < originalImage.Height; y++)
                    {
                        newImageGraphics.FillRectangle(new SolidBrush(originalImage.GetPixel(x, y)), x * ratio, y * ratio, x + ratio, y + ratio);
                    }
                }
            }
            //Return processed image
            return newImage;
        }

        /// <summary>
        /// Scalling an image with interpolation and keeping ratio
        /// This is written by Alex Aza of Stackoverflow and is modified to handle rounding issues
        /// http://stackoverflow.com/questions/6501797/resize-image-proportionally-with-maxheight-and-maxwidth-constraints
        /// </summary>
        /// <param name="image">Original image</param>
        /// <param name="maxWidth">Maximum desired width</param>
        /// <param name="maxHeight">Maximum desired height</param>
        /// <returns>A scaled image</returns>
        public static Image ScaleImageWithInterpolation(Image image, int maxWidth, int maxHeight)
        {
            var ratioX = (double)maxWidth / image.Width;
            var ratioY = (double)maxHeight / image.Height;
            var ratio = Math.Min(ratioX, ratioY);

            var newWidth = (int)Math.Ceiling((image.Width * ratio));
            var newHeight = (int)Math.Ceiling((image.Height * ratio));

            var newImage = new Bitmap(newWidth, newHeight);

            using (var graphics = Graphics.FromImage(newImage))
                graphics.DrawImage(image, 0, 0, newWidth, newHeight);

            return newImage;
        }

        #endregion

    }
}
